export class User {
  public id?: number = null;
  public username?: string = null;
  public password?: string = null;

  constructor(obj: User = {} as User) {
    const {
      id,
      username,
      password
    } = obj;

    this.id = id;
    this.username = username;
    this.password = password;
  }
}
