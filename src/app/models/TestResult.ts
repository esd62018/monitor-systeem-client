export class TestResult {
  public status?: number = null;
  public name?: string = null;
  public error?: string = null;
  public result?: string = null;
  public method?: string = null;

  constructor(obj: TestResult = {} as TestResult) {
    const {
      status,
      name,
      error,
      result,
      method
    } = obj;

    this.status = status;
    this.name = name;
    this.error = error;
    this.result = result;
    this.method = method;
  }
}
