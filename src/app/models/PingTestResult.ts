export class PingTestResult {
  public status?: number = null;
  public name?: string = null;
  public error?: string = null;

  constructor(obj: PingTestResult = {} as PingTestResult) {
    const {
      status,
      name,
      error
    } = obj;

    this.status = status;
    this.name = name;
    this.error = error;
  }
}
