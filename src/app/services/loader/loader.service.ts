import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoaderService {
  private _showSubject: Subject<boolean>;
  private _show: Observable<boolean>;

  constructor() {
    this._showSubject = new Subject<boolean>();
    this._show = this._showSubject.asObservable();
  }

  get show(): Observable<boolean> {
    return this._show;
  }

  toggleLoader() {
    this._showSubject.next(!this.show);
  }

  showLoader(): void {
    this._showSubject.next(true);
  }

  hideLoader(): void {
    this._showSubject.next(false);
  }
}
