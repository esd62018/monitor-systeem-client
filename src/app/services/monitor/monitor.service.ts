import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';
import { SnackBarService } from '../snack-bar/snack-bar.service';
import { LoaderService } from '../loader/loader.service';

import { TestResult } from '../../models/TestResult';
import { PingTestResult } from '../../models/PingTestResult';

@Injectable()
export class MonitorService {

  constructor(
    private http: HttpClientService,
    private router: Router,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  getDrivingApiData(): Observable<TestResult[]> {
    return this.http.get<TestResult[]>("drivers-test-rest");
  }

  getPoliceApiData(): Observable<TestResult[]> {
    return this.http.get<TestResult[]>("police-test-rest");
  }

  getRegistrationApiData(): Observable<TestResult[]> {
    return this.http.get<TestResult[]>("registration-test-rest");
  }

  getGovernmentApiData(): Observable<TestResult[]> {
    return this.http.get<TestResult[]>("government-test-rest");
  }

  getOtherCountriesApiData(): Observable<PingTestResult[]> {
    return this.http.get<TestResult[]>("other-countries-tests");
  }
}
