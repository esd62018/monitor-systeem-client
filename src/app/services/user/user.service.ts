import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';
import { SnackBarService } from '../snack-bar/snack-bar.service';
import { LoaderService } from '../loader/loader.service';

import { User } from '../../models/User';
import * as AuthStore from '../../utils/AuthStore';

@Injectable()
export class UserService {
  private _auth: User = null;

  constructor(
    private http: HttpClientService,
    private router: Router,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  get auth(): User {
    return this._auth;
  }

  authenticate(): void {
    this.loaderService.showLoader();

    const { auth, token, expireDate } = AuthStore.getAuthStoreData();

    if (auth && token && expireDate) {
      this.login(new User(auth)).subscribe(
        () => {},   // ignore (handled by login)
        () => this.snackBarService.showOops(),
        () => this.loaderService.hideLoader()
      );
    }
  }

  login(user: User): Observable<User> {
    return this.http
      .post<any>('auth/login', user)
      .pipe(
        tap(data => {
          if (data && data.auth && data.token && data.expireDate) {
            const { auth, token, expireDate } = data;

            this._auth = new User(auth);

            HttpClientService.setTokenHeader(token);

            AuthStore.setAuthStoreData(token, expireDate, this._auth);

            this.router.navigate(['/dashboard', { dashboard: ['monitor'] }]);
          }
        })
      );
  }

  logout() {
    AuthStore.clearAuthStoreData();

    HttpClientService.clearTokenHeader();

    this.router.navigate(['/login']);
  }
}
