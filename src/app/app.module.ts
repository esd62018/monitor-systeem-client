import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './modules/material/material.module';

import { AppComponent } from './app.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MonitorComponent } from './components/monitor/monitor.component';

import { HttpClientService } from './services/http-client/http-client.service';
import { UserService } from './services/user/user.service';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { SnackBarService } from './services/snack-bar/snack-bar.service';
import { LoaderService } from './services/loader/loader.service';
import { MonitorService } from './services/monitor/monitor.service'

@NgModule({
  declarations: [
    AppComponent,
    SnackBarComponent,
    LoaderComponent,
    LoginComponent,
    DashboardComponent,
    MonitorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpClientService,
    UserService,
    AuthGuardService,
    SnackBarService,
    LoaderService,
    MonitorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
