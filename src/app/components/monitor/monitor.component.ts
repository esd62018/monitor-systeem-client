import { Component, OnInit } from '@angular/core';

import { MonitorService } from '../../services/monitor/monitor.service'
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { TestResult } from '../../models/TestResult';
import { PingTestResult } from '../../models/PingTestResult';

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss']
})
export class MonitorComponent implements OnInit {

  private INTERVAL_TIME_OUT = 900000;

  driversApiData: TestResult[];

  governmentApiData: TestResult[];

  policeApiData: TestResult[];

  registerApiData: TestResult[];

  otherCountriesApiData: PingTestResult[];

  constructor(
    private monitorService: MonitorService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) {

  }

  ngOnInit() {
    // initial fetch
    this.fetchData();

    // Scheduled fetch
    setInterval(() => {
      this.fetchData();
    }, this.INTERVAL_TIME_OUT)
  }

  fetchData(): void {
    this.getDriversApiData();
    this.getGovernmentApiData();
    this.getPoliceApiData();
    this.getOtherCountriesApiData();
  }

  public getDriversApiData(): void {
    this.loaderService.showLoader();

    this.monitorService
      .getDrivingApiData()
      .subscribe(
        result => this.driversApiData = result,
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  getGovernmentApiData(): void {
    this.loaderService.showLoader();

    this.monitorService
      .getGovernmentApiData()
      .subscribe(
        result => this.governmentApiData = result,
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  getPoliceApiData(): void {
    this.loaderService.showLoader();

    this.monitorService
      .getPoliceApiData()
      .subscribe(
        result => this.policeApiData = result,
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  getRegisterApiData(): void {
    this.loaderService.showLoader();

    this.monitorService
      .getRegistrationApiData()
      .subscribe(
        result => this.registerApiData = result,
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  getOtherCountriesApiData(): void {
    this.loaderService.showLoader();

    this.monitorService
      .getOtherCountriesApiData()
      .subscribe(
        result => this.otherCountriesApiData = result,
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
